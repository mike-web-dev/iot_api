# README #

Setting up an IoT API 

# How to Set up mLab #

mLab managed MongoDB with api

###### List databases ######

GET /databases

Example:
```
https://api.mlab.com/api/1/databases?apiKey=myAPIKey
```

List collections

GET /databases/{database}/collections
```
Example:
https://api.mlab.com/api/1/databases/my-db/collections?apiKey=myAPIKey
```

List documents

GET /databases/{database}/collections/{collection}

```
Example listing all documents in a given collection:
https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey

Optional parameters
[q=<query>][&c=true][&f=<fields>][&fo=true][&s=<order>][&sk=<skip>][&l=<limit>]
```
```
"q" example - return all documents with "active" field of true:
https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey
```

Insert document

POST /databases/{database}/collections/{collection}
Content-Type: application/json
Body: <JSON data>

```
Example (using jQuery):

$.ajax( { url: "https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey",
		  data: JSON.stringify( { "x" : 1 } ),
		  type: "POST",
		  contentType: "application/json" } );
```

### GIS ###
https://www.qgis.org/en/site/

### MongoDB ###
* https://www.mongodb.com/
* http://haafiz.me/development/using-laravel-with-mongodb
* https://github.com/jenssegers/laravel-mongodb

### PostgreSQL / PostGIS ###
* https://www.postgresql.org/
* https://postgis.net/
* https://github.com/njbarrett/laravel-postgis

### Web Mapping Libraries ###
* http://leafletjs.com/
* https://developers.google.com/maps/
* https://openlayers.org/

### Geospatial Extentions ###
* http://geojson.org/
* http://jmikola.github.io/geojson/
* http://turfjs.org/
* http://www.gdal.org/

### Guides ###
* https://www.udemy.com/api-development/
* https://devcenter.heroku.com/articles/mean-apps-restful-api
* http://www.bogotobogo.com/MEAN-Stack/Building-REST-API-with-Node-Mongodb.php
* https://www.djamware.com/post/5a0673c880aca7739224ee21/mean-stack-angular-5-crud-web-application-example
* https://www.youtube.com/watch?v=lCYMcYeGl-Q

### Mean Stack ###
* https://bitnami.com/stack/mean